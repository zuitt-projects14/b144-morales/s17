console.log("Hello World");

//Basic array structure
//Access Elements in an Array


//Two ways to initialize an array

let array = [1, 2, 3];
console.log(array);

let arr = new Array(1, 2, 3);
console.log(arr[0]);

let count = ["one", "two", "three", "four"];
/*
console.log(count[count.length - 1])
count[count.length - 1]*/


//Push method array.push();

count.push("element");
console.log(count);


function pushMethod(name){
	return count.push(name)
}

pushMethod("six");
pushMethod("seven");
pushMethod("eight");
console.log(count);

//pop can remove last element
function popMethod(){
	return count.pop()
}


console.log(popMethod());


//Unshift Method array.unshift() dagdag
/*count.unshift("hugot");

function unshift(name){
	return count.unshift(name);
}*/

//shift method bawas sa una

function shift(name){
	return count.shift(name);
}


//Sort method
let nums = [15, 32, 61, 130, 230, 13, 34];

nums.sort();
console.log(nums)

//Sort nums in ascending order

nums.sort(
	function(a, b){
		return a - b
	}
	)
//descending order
nums.sort(
	function(a, b){
		return b - a


	}
	)

console.log(nums);


//Reverse method array.reverse()
nums.reverse()


console.log(nums);

/*Splice and Slice*/

//Splice method array.splice();



/*

//returns an array of omiited elements

first parameter - index where to start omitting element
//it directly manipulates the original array
//Second parameter - # of elements to be omitted from first parameter.
//third parameter - elements to be added in the place of the omitted elements
*/


/*let newSplice = count.splice(1);

console.log(newSplice)
*/


/*let newSplice = count.splice(1, 2);
console.log(newSplice);
console.log(count);*/

/*let newSplice = count.splice(1, 2, "a1", "b2", "c3")

console.log(newSplice);
console.log(count);*/

//Slice method - array.slice(start, end)
/*
	//first parameter - index where to begin omitting elements in an array
	//second parameter - # of elements to be omitted (index - array.lenght)

*/
//iniistore niya is yung natirang element
//hindi binabago ang original array//photocopy lang siya
/*console.log(count);*/
/*let newSlice = count.slice(1)
console.log(newSlice);
console.log(count);*/
console.log(count);

let newSlice = count.slice(2, 3);
console.log(newSlice)

//Concat Method - array.concat()
	//use to merge two or more arrays
console.log(count);
console.log(nums);
let animals = ["bird", "cat", "dog", "fish"];

let newConCat = count.concat(nums, animals)
console.log(newConCat);

//Join method array.join()
//pwedeng empty parameter  or "", "-", " ",
let meal = ["rice", "steak", "juice"];

let newJoin = meal.join();
console.log(newJoin);

newJoin = meal.join(" ")
console.log(newJoin);

//toString method

console.log(nums)

/*console.log(nums[3].toString());*/
console.log(typeof nums[3]);
let newString = nums.toString();
console.log(typeof newString)

/*Accessors*/
let countries = [
"US", "PH", "CAN", "PH", "SG", "HK", "PH", "NZ"
];

//indexOf() array.indexOf()
//Returning the number of index number
//finds the index of a given element where it is "first" found.
let index = countries.indexOf("PH");
console.log(index);
//if element is not exist will return -1
let example = countries.indexOf("AU");
console.log(index);

//lastIndexOf()
//returning last last index number of the choosen element
//start counting with zero
let lastIndex = countries.lastIndexOf("PH");
console.log(lastIndex);


/*if(countries.indexOf("CAN") == -1){
	console.log(`Element not existing`)
} else {
	console.log(`Element exists in the countries array`)
}*/



/*Iterators*/
//forEach(cb()) array.forEach()
//map() array.map()


//forEach only displaying the elements
let days = ["monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"];
//forEach is to loop each element
//displaying one by one
//can't use return
days.forEach(
	function(element){
			console.log(element)
		}
	)


//map
//return copy of array from the original array which can be manipulated
let mapDays = days.map(
	function(day){
		return `${day} is the day og the week`
		}
	)

console.log(mapDays);
console.log(days);


/*Mini Activity*/

let days2 = [];

days.forEach(
	function(day){
		days2.push(day)
	}
	)
console.log(days2);

//filter array.filter(cb())
//same as foreach isa isa din
console.log(nums);

let newFilter = nums.filter(
	function(num){
		return /*condition*/num < 50

	}
	)
console.log(newFilter);


//includes array.includes()
//returns boolean value if a word is existing in the array
console.log(animals);

let newIncludes = animals.includes("dog")


console.log(newIncludes);

/*Mini Activity*/

function newAnimals(animal){

	if(animals.includes(animal) == true){
		return `${animal} is found`
	}
	else{
		return `${animal} not found`
	}
}



console.log(newAnimals("dog"));
console.log(newAnimals("fish"));
console.log(newAnimals("carabao"));


//every and some almost same as includes return value but every and some only return value if condition is met.

//every
	//returns boolean value
	//mejo strict
	//returns true only if all elements passed the given condition

	let newEvery = nums.every(
		function(num){
			return (num > 10)
		}
		)

	console.log(newEvery);

//Some parang for some(cb())
	//boolean
let newSome = nums.some(
	function( num ){
	return (num > 50)
	}
	)
/*let newSome2 = newSome2 = nums.some(num => num > 50)
console.log(newSome2);*/

//reduce(cb(previous, current))
let newReduce = nums.reduce(
	function(a, b){
		return a + b
	}
	)
console.log(newReduce);

//to get the average of nums array
//total all the elements
//divide it by total number of elements
let average = newReduce / nums.length

//toFixed (# of decimals) - return string

console.log(average.toFixed(2))

//parseInt() or parseFloat()

console.log(parseInt(average.toFixed(2)))
console.log(parseFloat(average.toFixed(2)));











	

































































































































